Presentation plan
-----------------

Michael
1) Introdution on Big Data and context

Jorge
2) HDFS internals
3) YARN scheduling
4) MR scheduling

Michael
5) Spark Scheduling
6) recap

Questions
---------

1) How locality works inside Yarn scheduler
2) What is the spark executor lifetime?

